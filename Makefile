
CC = gcc
LIB = pthread
HEADERS = main.h

demo: main.c $(HEADERS)
	$(CC) main.c -l$(LIB)	\
		-o demo
