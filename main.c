#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "main.h"



pthread_t cnst;
pthread_t car;

pthread_mutex_t lock;
#define acquire(lock)	while (pthread_mutex_lock(&lock)) ;
#define release(lock)	while (pthread_mutex_unlock(&lock)) ;

pthread_cond_t cvar[2];

/* We know there are two lanes in the bridge.
 *   in_lane[i] records the number of cars in the lane at the current time.
 *   available[i] records which lane is available
 *
 * Becase, we want to prevent starving. The constructor will construct another lane after a period of time.
 * But before we open another lane, we must first wait the cars in the current lane to cross to bridge 
 * at the same time prevent coming cars entering this lane. 
 * That means there would be a short period of time, that both lane are marked unavailable.
 * 
 * current_lane means the lane which is currently open 
 * (including the time when both lane are unavailable, but there are cars in it)
 */

bool available[2];
int current_lane = 0, in_lane[2];
#define another_lane	(1 - current_lane)


/* This is the thread for constructor.
 * They go to construct another lane after every time, 
 * so that no direction will starve.
 */
void* constructor_thread(void* args)
{
	int i = 0;

	while (1) {
		if (i ++ > 80) {
			available[another_lane] = true;
			break;
		}	// assuming the construction is done


		usleep(millisec(100));	//assuming he is doing the construction instead of sleeping

		acquire(lock);
		available[current_lane] = false;	
		release(lock);

		while (in_lane[current_lane] != 0) ;	//wait for all cars in the current_lane to go through
		
		acquire(lock);
		current_lane = another_lane;
		pthread_cond_broadcast(&cvar[current_lane]);
		printf("\n===============Constructor: I'll open the lane#%d=============\n", current_lane);
		available[current_lane] = true;
		release(lock);
	}

	return NULL;
}

char* city[] = {"Hanover", "Norwich"};
/* The car thread */
void* car_thread(void* lane)
{
	int i = (int) lane, j;

	printf("Car: I'm going to %s. Waiting for entering\n", city[i]);
	acquire(lock);
	while ( !available[i] || (in_lane[i] == MAX_CARS) )
		pthread_cond_wait(&cvar[i], &lock);

	printf("Car: I'm going to %s. And I'm entering the bridge now. There are %d cars in the bridge\n", city[i], in_lane[i] + 1);

	in_lane[i] ++;
	release(lock);

	usleep(millisec(14));

	acquire(lock);

	printf("Car: I've arrived %s\n", city[i]);
		
	in_lane[i] --;
	pthread_cond_signal(&cvar[i]);
	release(lock);

	return NULL;
}


/* set the seed for rand() which is used to create cars randomly */
void rand_init()
{
	time_t t;
	srand((unsigned) time(&t));	//set rand seed
}


int main()
{
	int i, j, k = 0;	

	//Initilization
	rand_init();

	if (pthread_mutex_init(&lock, NULL)) {	//prepare the lock
		fprintf(stderr, "Error: can\'t init a mutex\n");
		exit(-1);
	}

	if (pthread_cond_init(&cvar[0], NULL) ||	//prepare the condition var
		pthread_cond_init(&cvar[1], NULL)) {
		fprintf(stderr, "Error, can\'t init cvar\n");
		exit(-1);
	}

	// let the lane 0 (to Hanover) open first
	current_lane = 0;
	available[current_lane] = true;
	available[another_lane] = false;
	in_lane[0] = 0; 
	in_lane[1] = 0;

	pthread_create(&cnst, NULL, constructor_thread, NULL);


	/* create several cars every 20ms */
	while (1) {
		if (k ++ > 400)
			break;

		j = rand() % 12 + 1; 

		for (i = 0; i < j; i ++)
			pthread_create(&car, NULL, car_thread, (void*) (rand() & 1));
			/* if it fail, it could implicate that there are alread too much cars, so no need to add more */

		usleep(millisec(20));
	}

	pthread_join(cnst, NULL);
}
