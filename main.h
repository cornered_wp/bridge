#ifndef __MAIN__H__
#define __MAIN__H__


#define true	1
#define false	0
#define bool	unsigned char

#define MAX_CARS	10

#define milliToMic	1000
#define millisec(x)	(x * milliToMic)	
// This give the argument to usleep so that the thread will sleep x milli sec

#endif
